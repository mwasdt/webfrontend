#MWA Monitoring Web Interface
  
>Monitoring the Murchison Widefield Array Metadata for Antenna Performance and Health  

>Curtin University - Capstone Computing Project One  
Group 9.2 - 2017

##Installation  
Requires Node >=4.2.6 and npm >=4.0.0

- Clone the repo.  
`git clone https://gene_cross@bitbucket.org/curtinmwamonitoringteam/webfrontend.git`

- Install the required modules.  
`npm install`

##Usage
While in the top directory:  

Run the server with:  
`npm start`  

Run the tests with:  
`npm test`

##Deployment
Coming soon.

##Documentation
Coming soon.

##License
All rights reserved.

