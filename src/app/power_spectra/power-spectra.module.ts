import {NgModule} from '@angular/core';
import {PowerSpectraComponent} from './power-spectra.component';
import {PowerSpectraRibbonComponent} from "./power-spectra-ribbon.component";
import {PowerSpectraCardComponent} from "./power-spectra-card.component";
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [
    PowerSpectraComponent,
    PowerSpectraRibbonComponent,
    PowerSpectraCardComponent,
  ],
  exports: [
    PowerSpectraComponent,
  ]

})
export class PowerSpectraModule {

}