import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'mwa-power-spectra-ribbon',
  templateUrl: 'power-spectra-ribbon.component.html',
  styleUrls: ['power-spectra-ribbon.component.css']
})
export class PowerSpectraRibbonComponent {

  filterButtons: {label: string, func: Function}[] = [];
  cardSizeButtons: {label: string, size: number}[] = [];
  @Output() cardSizeEvent = new EventEmitter<number>();

  ngOnInit(): void {
    this.filterButtons.push({label:'Date Range', func: this.dateRange});
    this.filterButtons.push({label:'Health Report No', func: this.dateRange});
    this.filterButtons.push({label:'Job No', func: this.dateRange});
    this.filterButtons.push({label:'Anomaly Type', func: this.dateRange});

    this.cardSizeButtons.push({label:'Small', size:200});
    this.cardSizeButtons.push({label:'Medium', size:400});
    this.cardSizeButtons.push({label:'Large', size:600});
  }

  dateRange(): void {
  }

  setCardSize(size: number): void {
    this.cardSizeEvent.emit(size);
  }

}
