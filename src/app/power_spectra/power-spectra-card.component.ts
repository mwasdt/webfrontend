import { Component, Input } from '@angular/core';

@Component({
  selector: 'mwa-power-spectra-card',
  templateUrl: 'power-spectra-card.component.html',
  styleUrls: ['power-spectra-card.component.css'],
})
export class PowerSpectraCardComponent {

  @Input() width: number;
  @Input() height: number;
  @Input() label: string;

}
