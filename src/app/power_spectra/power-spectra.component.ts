import { Component } from '@angular/core';
import { CardData } from './card-data.class';

@Component({
  selector: 'mwa-power-spectra',
  templateUrl: 'power-spectra.component.html',
  styleUrls: ['power-spectra.component.css'],
})
export class PowerSpectraComponent {

  cardSize: number;
  rowSize: number;
  rows: number;
  cardList: CardData[] = [];

  ngOnInit(): void {
    this.cardSizeEventHandler(400);
    this.updateCardSpace();
  }

  cardSizeEventHandler(size: number): void {
    if (size > 0) {
      this.cardSize = size;
      this.rowSize = Math.floor(document.getElementById("cardSpace").clientWidth / this.cardSize);
      this.rows = Math.floor(document.getElementById("cardSpace").clientHeight / this.cardSize);
      this.updateCardSpace();
    }
  }

  updateCardSpace() {
    // clear cardList
    this.cardList = [];

    // add new cards
    for (let ii = 0; ii < this.rowSize * this.rows; ii++) {
      this.cardList.push(new CardData(ii.toString()));
    }
  }

}
