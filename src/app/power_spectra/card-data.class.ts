export class CardData {
  label: string;

  constructor(label: string) {
    this.label = label;
  }

  getLabel(): string {
    return this.label;
  }
}
