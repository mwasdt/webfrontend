import { Component } from '@angular/core';

@Component({
  selector: 'mwa-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
}
