import { Component } from '@angular/core'; 

@Component({
  selector: 'mwa-statistics',
  templateUrl: 'statistics.component.html',
  styleUrls: ['statistics.component.css'],
})

export class StatisticsComponent {


  tiles: {tileNumber: number, anomalousDipoles: number, malfunctioningDipoles: number}[] = [];

  list: {type: string}[] = [];

  ngOnInit(): void {
    
    this.tiles.push({tileNumber: 4, anomalousDipoles: 3,  malfunctioningDipoles: 0});
    this.tiles.push({tileNumber: 15, anomalousDipoles: 1, malfunctioningDipoles: 2});
    this.tiles.push({tileNumber: 22, anomalousDipoles: 8, malfunctioningDipoles: 1});
  
    this.list.push({type: "Malfunction"});
    this.list.push({type: "anonmalyType1"});
    this.list.push({type: "anonmalyType2"});
    this.list.push({type: "anonmalyType3"});
    this.list.push({type: "anonmalyType4"});
  }
  }
