import { Component } from '@angular/core';

@Component({
    selector: 'mwa-statistics-ribbon',
    templateUrl: 'statistics-ribbon.component.html',
    styleUrls: ['statistics-ribbon.component.css'],
})
export class StatisticsRibbonComponent {

    filters: {label: string}[] = [];

    ngOnInit(): void {
        this.filters.push({label:'Date Range'});
        this.filters.push({label:'All Jobs'});
        this.filters.push({label:'Job #'});
    }
}
