import { NgModule } from '@angular/core';
import { StatisticsComponent } from './statistics.component';
import { StatisticsRibbonComponent } from './statistics-ribbon.component';
import { StatisticsTileDetailsComponent } from "./statistics-tile-details.component";
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  imports: [
    BrowserModule,
  ],
  declarations: [
    StatisticsComponent,
    StatisticsRibbonComponent,
    StatisticsTileDetailsComponent,
  ],
  exports: [
    StatisticsComponent,
  ]

})
export class StatisticsModule {

}
