import { Component } from '@angular/core';

@Component({
  selector: 'mwa-statistics-tile-details',
  templateUrl: 'statistics-tile-details.component.html',
  styleUrls: ['statistics-tile-details.component.css'],
})

export class StatisticsTileDetailsComponent {}