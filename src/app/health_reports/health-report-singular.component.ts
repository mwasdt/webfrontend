import { Component } from '@angular/core';
import {RoutingService} from "../navigation/routing.service";

@Component({
  selector: 'mwa-health-report-singular',
  templateUrl: 'health-report-singular.component.html',
  styleUrls: ['health-report-singular.component.css'],
})

export class HealthReportsSingularComponent {
  dipoleCounter: number = 0;

  dipoles: {num: number, tileNum: number, dipoleID: string, anomaly: string}[] = [];

  constructor(
    private routingService: RoutingService) {
  }

  routeTo(route: string): void {
    this.routingService.routeTo(route);
  }

  incrementDipole(): number {
    this.dipoleCounter += 1;
    return this.dipoleCounter;
  }

  ngOnInit(): void {
    this.dipoles.push({num: this.incrementDipole(), tileNum: 21, dipoleID: 'F', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 21,  dipoleID: 'A', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 76,  dipoleID: 'G', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 76,  dipoleID: 'E', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 76,  dipoleID: 'J', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 89,  dipoleID: 'B', anomaly: 'Flatline'});
    this.dipoles.push({num: this.incrementDipole(), tileNum: 92,  dipoleID: 'C', anomaly: 'Flatline'});
  }
}