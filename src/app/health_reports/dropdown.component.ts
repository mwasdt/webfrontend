import { Component } from '@angular/core';
import { RoutingService } from '../navigation/routing.service';

@Component({
  selector: 'mwa-health-report-dropdown',
  templateUrl: 'dropdown.component.html',
  styleUrls: ['dropdown.component.css'],
})

export class HealthReportsDropdownComponent {
  dropdownItems: {label: string, link: string}[] = [];

  showDropdown = false;

  constructor(
    private routingService: RoutingService) {
  }

  routeTo(route: string): void {
    this.routingService.routeTo(route);
  }

  getDisplayState() {
    if(this.showDropdown) {
      return 'block';
    } else {
      return 'none';
    }
  }

  ngOnInit(): void {
    this.dropdownItems.push({label: 'Health Report', link: 'health-report-singular'});
    this.dropdownItems.push({label: 'Power Spectra', link: 'power-spectra'});
  }
}