import { NgModule } from '@angular/core';
import { HealthReportsComponent } from './health-reports.component';
import { HealthReportsDropdownComponent } from './dropdown.component'
import { HealthReportsRibbonComponent } from './health-report-ribbon.component'
import { HealthReportsSingularComponent } from "./health-report-singular.component";
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  imports: [
    BrowserModule,
  ],
  declarations: [
    HealthReportsComponent,
    HealthReportsDropdownComponent,
    HealthReportsRibbonComponent,
    HealthReportsSingularComponent,
  ],
  exports: [
    HealthReportsComponent,
  ]

})
export class HealthReportModule {

}