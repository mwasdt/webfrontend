import { Component } from '@angular/core';

@Component({
    selector: 'mwa-health-report-ribbon',
    templateUrl: 'health-report-ribbon.component.html',
    styleUrls: ['health-report-ribbon.component.css'],
})
export class HealthReportsRibbonComponent {

    filters: {label: string}[] = [];

    ngOnInit(): void {
        this.filters.push({label:'Date Range'});
        this.filters.push({label:'Health Report #'});
    }
}