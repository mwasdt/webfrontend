import { Component } from '@angular/core';

@Component({
  selector: 'mwa-health-report',
  templateUrl: 'health-reports.component.html',
  styleUrls: ['health-reports.component.css'],
})

export class HealthReportsComponent {
  tableRows: {id: number, date: string}[] = [];

  ngOnInit(): void {
    this.tableRows.push({id: 3602, date: 'May 22, Monday, 2017'});
    this.tableRows.push({id: 3601, date: 'May 21, Sunday, 2017'});
    this.tableRows.push({id: 3600, date: 'May 20, Saturday, 2017'});
    this.tableRows.push({id: 3599, date: 'May 19, Friday, 2017'});
  }
}