import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './navigation/routing.module';
import { RoutingService } from './navigation/routing.service';
import { NavigationModule } from './navigation/navigation.module';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';


@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    NavigationModule,
  ],
  exports: [
    AppComponent,
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [
    RoutingService,
    {provide: APP_BASE_HREF, useValue: '/'},
  ]
})
export class AppModule {
}
