import { NgModule } from '@angular/core';
import { NavigationComponent } from './navigation-header.component';
import { HomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { HealthReportModule } from '../health_reports/health-reports.module';
import { PowerSpectraModule } from '../power_spectra/power-spectra.module';
import { StatisticsModule } from '../statistics/statistics.module';

@NgModule({
  imports: [
    BrowserModule,
    HealthReportModule,
    PowerSpectraModule,
    StatisticsModule,
  ],
  declarations: [
    HomeComponent,
    NavigationComponent,
  ],
  exports: [
    NavigationComponent,
  ],
})
export class NavigationModule {
}
