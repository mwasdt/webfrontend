import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationComponent } from './navigation-header.component';
import { RouterTestingModule } from '@angular/router/testing'
import { AppRoutingModule } from './routing.module';
import { HomeComponent } from './home.component';
import { PowerSpectraComponent } from '../power_spectra/power-spectra.component';
import { StatisticsComponent } from '../statistics/statistics.component';
import { HealthReportsComponent } from '../health_reports/health-reports.component';
import { RoutingService } from './routing.service';

describe('Navigation Header Component', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule,
        RouterTestingModule,
      ],
      declarations: [
        HealthReportsComponent,
        HomeComponent,
        NavigationComponent,
        PowerSpectraComponent,
        StatisticsComponent,
      ],
      providers: [
        RoutingService,
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(NavigationComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component instanceof NavigationComponent).toBe(true, 'Failed to create NavigationComponent');
  });


});
